### 目的
分析attach流程中各个阶段占比,包括下载so文件，解析符号表等
设备：rk3568
Host OS: linux

### related issue 
https://gitee.com/openharmony/third_party_llvm-project/issues/I5IG24?from=project-issue
https://gitee.com/openharmony/third_party_llvm-project/issues/I5IFO9?from=project-issue


### Description:
1. 通过增加loader日志，识别加载流程中从设备下载so的时间
```C++
Status PlatformOHOS::DownloadModuleSlice(const FileSpec &src_file_spec,
                                            const uint64_t src_offset,
                                            const uint64_t src_size,
                                            const FileSpec &dst_file_spec) {
  if (src_offset != 0)
    return Status("Invalid offset - %" PRIu64, src_offset);

  Log *log = GetLogIfAllCategoriesSet(LIBLLDB_LOG_PLATFORM);
  LLDB_LOGF(log, "download %s begin" ,src_file_spec.GetCString());
  auto ret = GetFile(src_file_spec, dst_file_spec);
  LLDB_LOGF(log, "download %s ", src_file_spec.GetCString());
  return ret;
}
```
2.日志如[attach_estimation_03.14](./logs/attach_estimation_03.14_20s.log)，从日志看整体attach流程20s
3.通过过滤获取download日志[download_estimation-03.14](./logs/attach_estimation_03.14_20s_download_duration.log)
4.通过脚本[sum_download.awk](./scripts/calc_downloadtime.awk) 可以看出下载流程占用了18.31s时间，总共131900K的文件，hdc下载速度:7327K/S

### HDC 改进点
1. [IO optimization for file send or recv](https://gitee.com/openharmony/developtools_hdc/pulls/576) HDC 通过优化USB模式，大文件单线程下载能达到35M/s左右

### LLDB 场景测试
1. LLDB主要是根据target进程依赖下载对应so文件，以com.ohos.settings为调试目标
2. 测试环境： PR576的RK3568构建镜像，HOST侧为Archlinux
3. 测试方法-1：
  - 准备下载[文件列表](./logs/download_files.txt)
  - 使用go脚本循环下载文件
  - 统计下载数据大小，耗时
4. 测试方法-2：
  - 使用lldb直接测试


#### 测试结果
1. 单线程测试日志结果:[1](./logs/hdc_1_thread_download_little_file.log)
  - elapsed time: 17.42s
  - download size: 116.704256M
  - rate: 6.699M/s
2. 多线程测试日志结果:[2](./logs/hdc_3_thread_download_little_file.log)
  - elapsed time: 17.18s
  - download size: 116.704256M
  - rate: 6.79M/s
3. 使用lldb直接测试结果[3](./logs/lldb_test_with_hdc_optimiation.log)
  - download size: 113.548 (以~/.lldb/module_cache/remote-ohos/<ID>目录统计为准)
  - elapsed time: 13.8396
  - rate: 8.20M/s

#### 测试脚本
1. 可以通过调整cmdChannel大小，模拟单线程或多线程下载
2. 测试脚本位置[test_hdc.go](./scripts/test_hdc.go)
  - ./scripts/test_hdc [hdc path] download_files.txt
3. 处理输出结果,汇总计算hdc统计下载数据大小:
```
grep "Size:" ./hdc_3_thread_download_little_file |awk -F, '{print $2}' | awk -F: '{sum+=$2};END{print sum}'
```

### How to reproduce 
```
# server side
# unix address: /data/data/patch.sock 
./lldb-server platform --server --listen unix-abstract:///data/data/patch.sock

# In client side, we can read lldb commands from .lldbinit or interactively
# explictly load init when start: ./lldb -s ~/.lldbinit

platform select remote-ohos
platform connect unix-abstract-connect:///data/data/patch.sock
attach 1503 # 1503 is pid of the appspwan application
```

### log before fixing

### log after fixing 

### need more testing on stability to check if loading is correct




