package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"sync"
	"time"
)

var command string = " file recv "
var local_path string = " /tmp"

func execHdc(cmdstr string) {
	cmd := exec.Command("sh", "-c", cmdstr)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin

	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func test1() {
	if len(os.Args) <= 1 {
		return
	}
	command = os.Args[1] + command
	fmt.Println(command)

	var wg sync.WaitGroup

	for i := 0; i < 2; i++ {
		wg.Add(1)
		go func() {
			execHdc(command)
			wg.Done()
		}()
	}

	wg.Wait()
}

func main() {
	if len(os.Args) <= 1 {
		return
	}
	f, err := os.Open(os.Args[2])
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()

	fscanner := bufio.NewScanner(f)
	fscanner.Split(bufio.ScanLines)

	var filelines []string
	for fscanner.Scan() {
		filelines = append(filelines, fscanner.Text())
	}

	var wg sync.WaitGroup
	cmdChannel := make(chan string)
	stopChannel := make(chan struct{})

	go func() {
		for {
			select {
			case cmd := <-cmdChannel:
				execHdc(cmd)
				wg.Done()
			case <-stopChannel:
				break
			}
		}
	}()

	fmt.Println(len(filelines))

	now := time.Now()
	for _, line := range filelines {
		cmd := os.Args[1] + command + line + local_path
		wg.Add(1)
		cmdChannel <- cmd
	}

	wg.Wait()
	fmt.Println("time elapsedTime:", time.Since(now)/time.Millisecond)
	close(stopChannel)
}
